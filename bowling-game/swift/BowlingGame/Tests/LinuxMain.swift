import XCTest

import BowlingGameTests

var tests = [XCTestCaseEntry]()
tests += BowlingGameTests.allTests()
XCTMain(tests)
