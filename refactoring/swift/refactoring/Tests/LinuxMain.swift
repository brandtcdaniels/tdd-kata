import XCTest

import RefactoringTests

var tests = [XCTestCaseEntry]()
tests += RefactoringTests.allTests()
XCTMain(tests)
