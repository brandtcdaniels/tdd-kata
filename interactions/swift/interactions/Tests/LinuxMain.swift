import XCTest

import InteractionsTests

var tests = [XCTestCaseEntry]()
tests += InteractionsTests.allTests()
XCTMain(tests)
