import XCTest

import StringCalculatorTests

var tests = [XCTestCaseEntry]()
tests += StringCalculatorTests.allTests()
XCTMain(tests)
